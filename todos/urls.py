from django.urls import path
from todos.models import TodoList, TodoItem

from todos.views import (
  TodoListView,
  TodoListDetailView,
  TodoListUpdateView,
  TodoListDeleteView,
  TodoListCreateView,
  TodoItemCreateView,
  TodoItemUpdateView,

)

urlpatterns = [
  path("", TodoListView.as_view(), name='todo_list'),
  path("<int:pk>/", TodoListDetailView.as_view(), name='todo_detail'),
  path("create/", TodoListCreateView.as_view(), name='todo_create'),
  path("<int:pk>/edit/", TodoListUpdateView.as_view(), name='todo_update'),
  path("<int:pk>/delete/", TodoListDeleteView.as_view(), name='todo_delete'),
  path("items/create/", TodoItemCreateView.as_view(), name='todoitem_create'),
  path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name='todoitem_update')
]
